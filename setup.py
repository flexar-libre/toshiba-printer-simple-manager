from setuptools import setup
import os
import io


def read(fname):
    return io.open(os.path.join(os.path.dirname(__file__), fname), 'r', encoding='utf-8').read()


requires = []

setup(
    name='toshiba-printer-simple-manager',
    version='0.0.1',
    zip_safe=False,
    author='Flexar S.R.L',
    author_email='sistemas@flexar.com.ar',
    description='Toshiba printer simple manager',
    long_description=read('README.md'),
    platforms='Posix; MacOS X; Windows',
    packages=['toshiba_printer_simple_manager'],
    keywords='flexar toshiba printer',
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11',
        'Topic :: Internet'
    ],
    python_requires='>=3.8',
    license='GPL-3',
    install_requires=requires,
)
