# Introducción

Conector con impresora de etiquetas Toshiba con lenguaje TPCL

## Información

El módulo abre un socket tcp ip a un puerto especificado, luego se pueden invocar comandos para diferentes impresoras.

## Ejecución

Instale el módulo luego comience a utilizarlo.

## Licencia

El proyecto fue realizado bajo la licencia GPL-3.
