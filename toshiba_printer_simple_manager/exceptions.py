"""Este módulo contiene los errores que puede invocar la impresora."""


class PrinterConnectionError(Exception):
    pass


class PrinterTimeoutError(Exception):
    pass


class PrinterJobInitError(Exception):
    pass


class PrinterJobError(Exception):
    pass


class PrinterJobFinishError(Exception):
    pass
