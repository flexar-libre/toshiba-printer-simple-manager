"""Contiene enumeraciones para poder establecer parámetros sin la necesidad de acordarse el
valor del campo en el comando."""
from enum import Enum


class Status(Enum):
    """Enumeración para el estado de la impresora."""
    ON_LINE = 0
    """La impresora esta online esperando comandos"""
    HEAD_OPEN_ON_LINE = 1
    """La impresora esta online pero tiene la tapa abierta"""
    PROCESSING_COMMAND = 2
    """La impresora esta procesando comandos"""
    PAUSE = 4
    """La impresora esta en pausa"""
    WAITING_TO_BE_STRIPPED = 5
    """La impresora esta esperando a que quiten la etiqueta"""
    COMMAND_ERROR = 6
    """La impresora no entiende el comando enviado"""
    COMMS_ERROR = 7
    """La impresora tiene un error con la comunicación RS-232"""
    PAPER_JAM = 11
    """La impresora tiene un atasco de papel"""
    CUTTER_ERROR = 12
    """La impresora dio un error de corte"""
    NO_PAPER = 13
    """La impresora no tiene papel"""
    NO_RIBBON = 14
    """La impresora no tiene ribbon"""
    HEAD_OPEN_DURING_IMPRESS = 15
    """Se abrió la tapa de la impresora durante la impresión"""
    HEAD_ERROR = 17
    """La impresora tiene un error de cabezal"""
    EXCESS_HEAD_TEMP = 18
    """La impresora tiene exceso de temperatura en el cabezal"""
    RIBBON_ERROR = 21
    """La impresora tiene un error en el ribbon"""
    COVER_OPEN = 24
    """La impresora tiene la cubierta levantada"""
    RIBBON_NEAR_END_ON_LINE = 27
    """El ribbon esta llegando al final de la line"""
    PAUSE_RIBBON_NEAR_END = 28
    """La impresora se pauso porque el ribbon se termino"""
    RIBBON_NEAR_END_OPERATING = 29
    """El ribbon termino su operación"""
    LABEL_ISSUE_COMPLETED_NORMALLY = 40
    """La impresora imprimió correctamente"""
    FEED_COMPLETED_NORMALLY = 41
    """La impresora encontró papel"""
    HEAD_BROKEN_DOTS_CHECK_NORMALLY = 0
    """La impresora tiene el cabezal normal"""
    SAVING = 55
    """La impresora esta guardando en memoria"""
    FLASH_WRITE_ERR = 50
    """La impresora no puso escribir en la flash"""
    FORMAT_ERROR = 51
    """La impresora no pudo formatear la flash"""
    FLASH_CARD_FULL = 54
    """La impresora tiene la memoria flash llena"""
    RFID_WRITE_ERROR = 61
    """La impresora no pudo escribir en el RFID"""
    RFID_ERROR = 62
    """La impresora dió error en el RFID"""
    NOT_FOUND = 100
    """La impresora no existe"""


class StatusType(Enum):
    """Enumeración para el tipo de status."""
    RESPONSE = 1
    """Respuesta de estado solicitada"""
    AUTOMATIC = 2
    """Respuesta de estado automática"""


class FontTypeBitmap(Enum):
    """Enumeración para el tipo de fuente de un texto
    bitmap."""
    TIMES_ROMAN_MEDIUM_8PT = 'A'
    """Fuente de tipo Times Roman Medium 8pt"""
    TIMES_ROMAN_MEDIUM_10PT = 'B'
    """Fuente de tipo Times Roman Medium 10pt"""
    TIMES_ROMAN_BOLD_10PT = 'C'
    """Fuente de tipo Times Roman Bold 10pt"""
    TIMES_ROMAN_BOLD_12PT = 'D'
    """Fuente de tipo Times Roman Bold 12pt"""
    TIMES_ROMAN_BOLD_14PT = 'E'
    """Fuente de tipo Times Roman Bold 14pt"""
    TIMES_ROMAN_ITALIC_12PT = 'F'
    """Fuente de tipo Times Roman Italic 12pt"""
    HELVETICA_MEDIUM_6PT = 'G'
    """Fuente de tipo Helvetica Medium 6pt"""
    HELVETICA_MEDIUM_10PT = 'H'
    """Fuente de tipo Helvetica Medium 10pt"""
    HELVETICA_MEDIUM_12PT = 'I'
    """Fuente de tipo Helvetica Medium 12pt"""
    HELVETICA_BOLD_12PT = 'J'
    """Fuente de tipo Helvetica Bold 12pt"""
    HELVETICA_BOLD_14PT = 'K'
    """Fuente de tipo Helvetica Bold 14pt"""
    HELVETICA_ITALIC_12PT = 'L'
    """Fuente de tipo Helvetica Italic 12pt"""


class FontTypeOutline(Enum):
    """Enumeración para el tipo de fuente en un texto outline."""
    HELVETICA = 'A'
    """Tipo de fuente Helvetica"""
    HELVETICA_PROPORTIONAL = 'B'
    """Tipo de fuente Helvetica Proportional"""
    PRICE_FONT1 = 'E'
    """Tipo de fuente Price 1"""
    PRICE_FONT2 = 'F'
    """Tipo de fuente Price 2"""
    PRICE_FONT3 = 'G'
    """Tipo de fuente Price 3"""
    TIMES_ROMAN_BOLD_PROPORTIONAL = 'H'
    """Tipo de fuente Times Roman Bold Proportional"""
    POP_REGULAR_PROPORTIONAL = 'I'
    """Tipo de fuente Pop regular Proportional"""
    BLACK_PROPORTIONAL = 'J'
    """Tipo de fuente Back Proportional"""


class IssueMode(Enum):
    """Enumeración para el tipo de impresión de etiquetas."""
    BATCH_MODE = 'C'
    """Modo de batch"""
    STRIP_MODE_SENSOR_ON = 'D'
    """Modo de strip con sensor"""
    STRIP_MODE_SENSOR_OFF = 'E'
    """Modo de strip sin sensor"""


class FontAngle(Enum):
    """Enumeración para el ángulo de la fuente."""
    DEGREE_0 = "00"
    """Rotación de 0 grados"""
    DEGREE_90 = "11"
    """Rotación de 90 grados"""
    DEGREE_180 = "22"
    """Rotación de 180 grados"""
    DEGREE_270 = "33"
    """Rotación de 360 grados"""


class CharacterAttribution(Enum):
    """Enumeración para el tipo de character attribution."""
    BLACK = 'B'
    """Tipo Back"""
    REVERSE = 'W'
    """Tipo Reverse"""
    BOXED = 'F'
    """Tipo Boxed"""
    STROKED = 'C'
    """Tipo Stroked"""


class TypeOfSensor(Enum):
    """Enumeración para el sensor de uso en la impresión."""
    NO_SENSOR = 0
    """Sin sensor"""
    REFLECTIVE = 1
    """Con sensor reflectivo"""
    TRANSMISSIVE = 2
    """Con sensor transmisivo"""
    TRANSMISSIVE_MANUAL_THRESHOLD = 3
    """Con sensor transmisivo con umbral manual"""
    REFLECTIVE_MANUAL_THRESHOLD = 4
    """Con sensor reflectivo con umbral manual"""


class PrintSpeed(Enum):
    """Enumeración para la velocidad de la impresión."""
    IPS_2 = 2
    """2 pulgadas por segundo"""
    IPS_4 = 4
    """4 pulgadas por segundo"""
    IPS_6 = 6
    """6 pulgadas por segundo"""


class Ribbon(Enum):
    """Enumeración para el uso del ribbon."""
    NO_RIBBON = 0
    """Sin ribbon"""
    RIBBON1 = 1
    """Con ribbon 1"""
    RIBBON2 = 2
    """Con ribbon 2"""


class PrintOrientation(Enum):
    """Enumeración de la impresión de la etiqueta."""
    BOTTOM_FIRST = 0
    """Primero desde abajo"""
    TOP_FIRST = 1
    """Primero desde arriba"""
    BOTTOM_FIRST_MIRROR = 2
    """Primero abajo pero reflejado"""
    TOP_FIRST_MIRROR = 3
    """Primero desde arriba pero reflejado"""


class GraphicsType(Enum):
    """Enumeración para el tipo de gráficos."""
    NIBBLE_OVERRIDE = 0
    """Formato de imagen 1 byte por cada 4 puntos"""
    HEX_OVERRIDE = 1
    """Formato de imagen 1 byte por cada 8 puntos"""
    BMP_OVERRIDE = 2
    """Formato de imagen BMP"""
    TOPIX_OVERRIDE = 3
    """Formato de imagen de compresión TOPIX"""
    NIBBLE_OR = 4
    """Formato de imagen 1 byte por cada 4 puntos en or"""
    HEX_OR = 5
    """Formato de imagen 1 byte por cada 8 puntos en or"""
    PCX_OVERRIDE = 6
    """Formato de imagen PCX"""
    TOPIX_XOR = 7
    """Formato de imagen de compresión TOPIX en or"""


class BarCodeType(Enum):
    """Enumeración para el tipo de código de barras."""
    MSI = '1'
    """Formato MSI"""
    ITF = '2'
    """Formato ITF"""
    CODE39_STANDARD = '3'
    """Formato CODE39"""
    NW7 = '4'
    """Formato NW7"""
    CODE39_FULL_ASCII = 'B'
    """Formato CODE39 FULL ASCII"""
    INDUSTRIAL = 'O'
    """Formato Industrial"""
    NEC_MATRIX = 'a'
    """Formato NEC"""


class CheckDigitType(Enum):
    """Enumeración para el tipo de Check digit."""
    WITHOUT_ATTACHING = 1
    """Sin anclado"""
    CHECK = 2
    """Modo Check"""
    CHECK_AUTOMATIC1 = 3
    """Modo Check automático 1"""
    CHECK_AUTOMATIC2 = 4
    """Modo Check automático 2"""
    CHECK_AUTOMATIC3 = 5
    """Modo Check automático 3"""


class BarCodeAngle(Enum):
    """Enumeración para la rotación de códigos de barras."""
    DEGREE_0 = 0
    """0 grados"""
    DEGREE_90 = 1
    """90 grados"""
    DEGREE_180 = 2
    """180 grados"""
    DEGREE_270 = 3
    """270 grados"""


class RectType(Enum):
    """Enumeración para el tipo de rect."""
    LINE = 0
    """Linea"""
    RECTANGLE = 1
    """Rectángulo"""
    LINE_DOTS_SKIPPED = 2
    """Linea con puntos"""
    RECTANGLE_DOTS_SKIPPED = 3
    """Rectángulo con puntos"""


class FormatArea(Enum):
    """Enumeración para el area de memoria a formatear."""
    ENTIRE_AREA = 'A'
    """Area de memoria completa"""
    PC_SAVE_AREA = 'B'
    """Area de memoria de PC"""
    CHARACTER_STORAGE_AREA = 'C'
    """Area de memoria de caracteres"""


class PrintMode(Enum):
    """Enumeración para el tipo de impresión a realizar."""
    THERMAL_TRANSFER = '0'
    """Transferencia térmica"""
    DIRECT_THERMAL = '1'
    """Transmisión directa"""
