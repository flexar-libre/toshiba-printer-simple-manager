"""Este módulo contiene todos los comandos ejecutables por la impresora."""
from .definitions import (FormatArea, FontTypeBitmap, CharacterAttribution,
                          FontAngle, FontTypeOutline, RectType,
                          TypeOfSensor, IssueMode, PrintSpeed, Ribbon, PrintOrientation,
                          PrintMode)


class Commands:
    """Clase con métodos estáticos que representan a los
    comandos que puede ejecutar la impresora. Esta clase
    provee una capa mínima para evitar escribir comandos
    TPCL manualmente."""

    @staticmethod
    def status() -> bytes:
        """Comando de estado de la impresora.

        Examples:
            >>> cmd = Commands.status()

        Returns:
            status (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return b'{WS|}'

    @staticmethod
    def version() -> bytes:
        """Comando de versión de la impresora.

        Examples:
            >>> cmd = Commands.version()

        Returns:
            version (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return b'{WV|}'

    @staticmethod
    def option() -> bytes:
        """Comando de estado de conexiones físicas de la impresora.

        Examples:
            >>> cmd = Commands.option()

        Returns:
            option (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return b'{WN|}'

    @staticmethod
    def data_save_start(num: int, status_response: bool) -> bytes:
        """Comando para iniciar el guardado de un formato de etiqueta
        en la impresora.

        Examples:
            >>> cmd = Commands.data_save_start(1, False)

        Parameters:
            num (int): Dirección de memoria en la cual se quiere guardar el formato
            status_response (bool): Establece si una vez enviado el comando, la impresora debe retornar una respuesta de estado en el trabajo de guardado

        Returns:
            data_save_start (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{XO;{},{}|}}'.format(
            str(num).rjust(2, '0'),
            1 if status_response else 0,
        ).encode()

    @staticmethod
    def data_save_stop() -> bytes:
        """Comando para finalizar el guardado de un formato de etiqueta
        en la impresora.

        Examples:
            >>> cmd = Commands.data_save_start(1, False)
            >>> cmd = Commands.set_label_size(1000, 200, 980)
            >>> cmd = Commands.data_save_stop()

        Returns:
            data_save_stop (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{XP|}'

    @staticmethod
    def data_read(num: int, status_response: bool,
                  automatic_call: bool) -> bytes:
        """Comando para leer un formato de etiqueta de la impresora.

        Examples:
            >>> cmd = Commands.data_read(1, False)

        Parameters:
            num (int): Dirección de memoria de la cual se quiere leer el formato
            status_response (bool): Establece si una vez enviado el comando, la impresora debe retornar una respuesta de estado en el trabajo de lectura
            automatic_call (bool): Establece si se debe leer este formato automáticamente cuando la impresora se enciende

        Returns:
            data_read (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{XQ;{},{},{}|}}'.format(
            str(num).rjust(2, '0'),
            1 if status_response else 0,
            'L' if automatic_call else 'M',
        ).encode()

    @staticmethod
    def storage_area_allocate(font_size: int, character_size: int,
                              basic_file_size: int) -> bytes:
        """Comando para asignar el área de almacenamiento en la ROM
        flash en la placa de la CPU.
        El tamaño se envía en múltiplos de 128KB.

        Parameters:
            font_size (int): Tamaño de 00 a 24 de la fuente
            character_size (int): Tamaño de 00 a 24 del carácter
            basic_file_size (int): Tamaño de 00 a 14 del archivo base

        Returns:
            storage_area_allocate (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{XF;{},{},{}|}}'.format(
            str(font_size).rjust(2, '0'),
            str(character_size).rjust(2, '0'),
            str(basic_file_size).rjust(2, '0'),
        ).encode()

    @staticmethod
    def flash_memory_format(area: FormatArea) -> bytes:
        """Comando para formatear la flash rom en la CPU.

        Parameters:
            area (FormatArea): Area de formateo

        Returns:
            flash_memory_format (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{J1;{}|}}'.format(
            area.value,
        ).encode()

    @staticmethod
    def display_print(msg: str) -> bytes:
        """Comando para imprimir un mensaje en el display de la impresora.
        Cuando el mensaje se muestra, la impresora de pone en pausa y el
        usuario debe despausarla manualmente en la botonera de impresora.
        Es por esto que se debe tener cuidado con este comando si la administración
        es remota.

        Examples:
            >>> cmd = Commands.display_print("Printer msg")

        Parameters:
            msg (str): Mensaje para imprimir en el display de la impresora

        Returns:
            display_print (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{XJ;{}|}}'.format(
            msg,
        ).encode()

    @staticmethod
    def eject() -> bytes:
        """Comando para ejectar una etiqueta.

        Examples:
            >>> cmd = Commands.eject()

        Returns:
            eject (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return b'{IB|}'

    @staticmethod
    def reset() -> bytes:
        """Comando para resetear la impresora.

        Examples:
            >>> cmd = Commands.reset()

        Returns:
            reset (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return b'{WR|}'

    @staticmethod
    def forward(mm: int) -> bytes:
        """Comando para mover una etiqueta una cantidad de mm hacia adelante.

        Examples:
            >>> cmd = Commands.forward(10)

        Parameters:
            mm (int): Distancia en mm que se moverá la etiqueta

        Returns:
            forward (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{U1;{}|}}'.format(
            str(mm).rjust(4, '0'),
        ).encode()

    @staticmethod
    def reverse(mm: int) -> bytes:
        """Comando para mover una etiqueta una cantidad de mm hacia atrás.

        Examples:
            >>> cmd = Commands.reverse(10)

        Parameters:
            mm (int): Distancia en mm que se moverá la etiqueta

        Returns:
            reverse (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{U2;{}|}}'.format(
            str(mm).rjust(4, '0'),
        ).encode()

    @staticmethod
    def clear_buffer() -> bytes:
        """Comando para limpiar el buffer gráfico de la impresora.

        Examples:
            >>> cmd = Commands.clear_buffer()

        Returns:
            clear_buffer (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return b'{C|}'

    @staticmethod
    def set_label_size(pitch: int, width: int, height: int) -> bytes:
        """Comando para establecer el tamaño de etiqueta.

        Examples:
            >>> cmd = Commands.set_label_size(1000, 500, 980)

        Parameters:
            pitch (int): Distancia vertical en mm entre una etiqueta y otra
            width (int): Tamaño horizontal en mm de la etiqueta
            height (int): Tamaño vertical en mm de la etiqueta

        Returns:
            set_label_size (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{D{},{},{}|}}'.format(
            str(pitch).rjust(4, '0'),
            str(width).rjust(4, '0'),
            str(height).rjust(4, '0'),
        ).encode()

    @staticmethod
    def set_ax(print: int, cut: int, feed: int) -> bytes:
        """Comando para establecer un ajuste de offset para cada impresión.

        Examples:
            >>> cmd = Commands.set_ax(-24, 0, 0)

        Parameters:
            print (int): Distancia en dmm para el offset en X
            cut (int): Distancia en dmm para el offset de corte
            feed (int): Distancia en dmm para el offset del reverse feed

        Returns:
            set_ax (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{AX;{}{},{}{},{}{}|}}'.format(
            '+' if print >= 0 else '-', str(abs(print)).rjust(3, '0'),
            '+' if cut >= 0 else '-', str(abs(cut)).rjust(3, '0'),
            '+' if feed >= 0 else '-', str(abs(feed)).rjust(2, '0'),
        ).encode()

    @staticmethod
    def set_ay(steps: int, mode: PrintMode) -> bytes:
        """Comando para establecer un ajuste de densidad para cada impresión.

        Examples:
            >>> cmd = Commands.set_ay(30, PrintMode.DIRECT_THERMAL)

        Parameters:
            steps (int): Número de pasos
            mode (int): Modo de impresión

        Returns:
            set_ay (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{AY;{}{},{}|}}'.format(
            '+' if steps >= 0 else '-', str(abs(steps)).rjust(2, '0'),
            mode.value
        ).encode()

    @staticmethod
    def bitmap_text(num: int, x: int, y: int,
                    horizontal_magnification: int,
                    vertical_magnification: int,
                    font_type: FontTypeBitmap,
                    rotate: FontAngle,
                    character_attribution: CharacterAttribution,
                    text: str) -> bytes:
        """Comando para añadir un texto con formato bitmap a la etiqueta.

        Examples:
            >>> cmd = Commands.bitmap_text(
                0, 100, 100, 10, 10,
                FontTypeBitmap.HELVETICA_MEDIUM_12PT,
                FontAngle.DEGREE_0,
                CharacterAttribution.BLACK,
                'Text in label'
            )

        Parameters:
            num (int): Número del campo de texto
            x (int): Distancia en el eje x en mm
            y (int): Distancia en el eje y en mm
            horizontal_magnification (int): Escala horizontal de la fuente
            vertical_magnification (int): Escala vertical de la fuente
            font_type (FontTypeBitmap): Familia de fuente
            rotate (FontAngle): Ángulo de la fuente
            character_attribution (CharacterAttribution): Character attribution del texto
            text (str): Valor del campo

        Returns:
            bitmap_text (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{PC{};{},{},{},{},{},{},{}{}|}}'.format(
            str(num).rjust(3, '0'),
            str(x).rjust(4, '0'),
            str(y).rjust(4, '0'),
            str(horizontal_magnification).rjust(2, '0'),
            str(vertical_magnification).rjust(2, '0'),
            font_type.value,
            rotate.value,
            character_attribution.value,
            '=' + text if text != '' else '',
        ).encode()

    @staticmethod
    def outline_text(num: int,
                     x: int, y: int,
                     width: int, height: int,
                     font_type: FontTypeOutline,
                     angle: FontAngle,
                     character_attribution: CharacterAttribution,
                     text: str) -> bytes:
        """Comando para añadir un texto con formato outline a la etiqueta.

        Examples:
            >>> cmd = Commands.outline_text(
                0, 100, 100, 300, 300,
                FontTypeOutline.HELVETICA_PROPORTIONAL,
                FontAngle.DEGREE_0,
                CharacterAttribution.BLACK,
                'Text in label'
            )

        Parameters:
            num (int): Número del campo de texto
            x (int): Distancia en el eje x en mm
            y (int): Distancia en el eje y en mm
            width (int): Tamaño horizontal del texto
            height (int): Tamaño vertical del texto
            font_type (FontTypeBitmap): Familia de fuente
            angle (FontAngle): Ángulo de la fuente
            character_attribution (CharacterAttribution): Character attribution del texto
            text (str): Valor del campo

        Returns:
            outline_text (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{PV{};{},{},{},{},{},{},{}{}|}}'.format(
            str(num).rjust(2, '0'),
            str(x).rjust(4, '0'),
            str(y).rjust(4, '0'),
            str(width).rjust(4, '0'),
            str(height).rjust(4, '0'),
            font_type.value,
            angle.value,
            character_attribution.value,
            '=' + text if text != '' else '',
        ).encode()

    @staticmethod
    def rect(xi: int, yi: int, xf: int, yf: int,
             rect_type: RectType, width: int, radius: int) -> bytes:
        """Comando para añadir un rectángulos o lineas a la etiqueta.

        Examples:
            >>> cmd = Commands.rect(
                100, 100, 500, 500,
                RectType.RECTANGLE,
                4, 10
            )
            >>> cmd = Commands.rect(
                100, 100, 300, 300,
                RectType.LINE,
                2, 0
            )

        Parameters:
            yi (int): Distancia inicial en el eje y en mm
            xi (int): Distancia inicial en el eje x en mm
            xf (int): Distancia final en el eje x en mm
            yf (int): Distancia final en el eje y en mm
            rect_type (RectType): Tipo de rect (rectángulo o linea)
            width (int): Grosor de la linea
            radius (int): Radio de esquinas del rectángulo

        Returns:
            rect (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{LC;{},{},{},{},{},{},{}|}}'.format(
            str(xi).rjust(4, '0'),
            str(yi).rjust(4, '0'),
            str(xf).rjust(4, '0'),
            str(yf).rjust(4, '0'),
            rect_type.value,
            str(width).rjust(2, '0'),
            str(radius).rjust(3, '0'),
        ).encode()

    @staticmethod
    def set_bitmap_text(num: int, text: str) -> bytes:
        """Comando para establecer el valor de un campo de tipo bitmap text.

        Examples:
            >>> cmd = Commands.set_bitmap_text(0, 'Value')

        Parameters:
            num (int): Número del campo
            text (str): Valor a reemplazar en el campo

        Returns:
            set_bitmap_text (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{RC{};{}|}}'.format(
            str(num).rjust(3, '0'),
            text,
        ).encode()

    @staticmethod
    def set_outline_text(num: int, text: str) -> bytes:
        """Comando para establecer el valor de un campo de tipo outline text.

        Examples:
            >>> cmd = Commands.set_outline_text(0, 'Value')

        Parameters:
            num (int): Número del campo
            text (str): Valor a reemplazar en el campo

        Returns:
            set_outline_text (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{RV{};{}|}}'.format(
            str(num).rjust(2, '0'),
            text
        ).encode()

    @staticmethod
    def set_barcode(num: int, code: str) -> bytes:
        """Comando para establecer el valor de un campo de tipo barcode.

        Examples:
            >>> cmd = Commands.set_barcode(0, '9876543210')

        Parameters:
            num (int): Número del campo
            code (str): Valor a reemplazar en el campo

        Returns:
            set_barcode (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{RB{};{}|}}'.format(
            str(num).rjust(2, '0'),
            code,
        ).encode()

    @staticmethod
    def issue(type_of_sensor: TypeOfSensor,
              issue_mode: IssueMode,
              print_speed: PrintSpeed,
              ribbon: Ribbon,
              print_orientation: PrintOrientation,
              cut_interval: int,
              number_copies: int,
              status_response: bool) -> bytes:
        """Comando para iniciar la impresión.

        Examples:
            >>> cmd = Commands.issue(
                TypeOfSensor.REFLECTIVE,
                IssueMode.STRIP_MODE_SENSOR_ON,
                PrintSpeed.IPS_6,
                Ribbon.RIBBON1,
                PrintOrientation.BOTTOM_FIRST,
                0, 1, True
            )

        Parameters:
            type_of_sensor (TypeOfSensor): Sensor de la impresora que se utilizará
            issue_mode (IssueMode): Modo de impresión
            print_speed (PrintSpeed): Velocidad de impresión
            ribbon (Ribbon): Ribbon que se utilizará
            print_orientation (PrintOrientation): Orientación de la etiqueta
            cut_interval (int): Intervalo de corte
            number_copies (int): Número de copias
            status_response (bool): Si deberá retornar el status de impresión

        Returns:
            issue (bytes): Comando resultante en bytes para enviar por el socket de conexión
        """
        return '{{XS;I,{},{}{}{}{}{}{}{}|}}'.format(
            str(number_copies).rjust(4, '0'),
            str(cut_interval).rjust(3, '0'),
            type_of_sensor.value,
            issue_mode.value,
            print_speed.value,
            ribbon.value,
            print_orientation.value,
            1 if status_response else 0,
        ).encode()
